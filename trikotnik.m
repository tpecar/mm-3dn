% Tadej Pecar, 63140188

% trenutna verzija Octave ima pri izrisu grafa nek race condition, ki lahko povzroci
% octave_base_value::convert_to_str_internal (): wrong type argument '<unknown type>'
% tako da je vcasih potrebno demo zagnati veckrat

function [T, pl] = trikotnik(K, h=10e-7, izris=false)
  % poisce najvecji mozni trikotnik za oglisca na podanih kroznicah
  % VHOD:
  %    K - 3x3 matrika iz treh stolpcnih vektorjev [pi; qi; ri], pri cemer je
  %         pi - x koordinata sredisca i-te kroznice
  %         qi - y koordinata sredisca i-te kroznice
  %         ri - polmer i-te kroznice
  %    h - korak pri gradientni metodi (privzeto 10e-7)
  %        njegova vrednost je mocno odvisna od primera, a v grobem sta si
  %        ploscina trikotnika ter korak v obratnem sorazmerju
  %        (za vecjo ploscino, tj. bolj medsebojno oddaljene/vecje kroznice
  %        potrebujemo manjsi korak)
  %         za povrsine do 10 je tipicno 10e-4 ok,
  %         za povrsine do 100 je tipicno 10e-6
  %    izris - ce naj metoda izrise razvoj gradienta ter naredi sliko
  %            trikotnika (privzeto ga ni)
  % IZHOD:
  %    T - 2x3 matrika iz treh stolpcnih vektorjev oblike [xi, yi], ki
  %        predstavljajo koordinate oglisc (A, B, C) trikotnika
  
  % dolocimo anonimno funkcijo, ki definira gradient za podan pi,qi,ri
  gradf = @(x) trik_ploscina_grad(K(:,1), K(:,2), K(:,3), x);
  % za zacetne t, u, v podamo razlicne vrednosti, da se izognemo robnim
  % primerom (kjer so konstante enake 0), pri katerih bi bila norma
  % premajhna
  [xmin, k, xint] = gradmet(gradf, h, [0;pi/2;pi], 10e-10, 10e3);
  
  if(izris)
    trikotnik_izris(K, xmin, xint);
  end
  % dolocimo rezultat
  T = [K(1,:)+K(3,:).*cos(xmin');
       K(2,:)+K(3,:).*sin(xmin')];
  pl = trik_ploscina(K(:,1),K(:,2),K(:,3), xmin);
  
endfunction
%!demo
%! K = [6, 3, 1; 1, 8, 8; 8, 12, 10];
%! [T, pl] = trikotnik(K, 10e-7, true)

% enakostranicni trikotnik
%!test
%! r=2;
%! K = [0, 0, 0; 0, 0, 0; r, r, r];
%! [T, pl] = trikotnik(K, 10e-4);
% za enakostranicni trikotnik lahko izpeljemo, da ce potegnemo
% daljici do oglisc enega izmed stranic, je kot pri srediscu 120, ob ogliscu
% pa 30 stopinj - to nam da zvezo med dolzino polmera ocrtanega kroga ter
% stranico ter posledicno ploscino
%! ple = (3*(r^2)*sqrt(3))/4;
%! assert(pl,ple,1e-8)

% enakostranicni trikotnik, druga pozicija
%!test
%! r=2;
%! K = [1, 1, 1; 1, 1, 1; r, r, r];
%! [T, pl] = trikotnik(K, 10e-4);
%! ple = (3*(r^2)*sqrt(3))/4;
%! assert(pl,ple,1e-8)

% enakostranicni trikotnik, drug polmer
%!test
%! r=17;
%! K = [0, 0, 0; 0, 0, 0; r, r, r];
%! [T, pl] = trikotnik(K, 10e-7);
%! ple = (3*(r^2)*sqrt(3))/4;
%! assert(pl,ple,1e-8)

% nicelne kroznice - pri teh korak niti nima pomena, ker bi moralo
% konvergirati v enem koraku
%!test
%! K = [0, 0, 2; 1, 0, 0; 0, 0, 0];
%! [T, pl] = trikotnik(K, 10e-1);
%! assert(pl,1,1e-8)

% nicelne kroznice, drug vrstni red
%!test
%! K = [2, 0, 0; 0, 1, 0; 0, 0, 0];
%! [T, pl] = trikotnik(K, 10e-1);
%! assert(pl,1,1e-8)

% nicelne kroznice, drug vrstni red
%!test
%! K = [0, 2, 0; 0, 0, 1; 0, 0, 0];
%! [T, pl] = trikotnik(K, 10e-1);
%! assert(pl,1,1e-8)

% dve fiksni tocki, ena kroznica - ta je nastavljena tako, da bo
% najvecja ploscina pri pravokotnem trikotniku
%!test
%! K = [0, 0, 2; 1, 0, 0; 0, 0, 1];
%! [T, pl] = trikotnik(K, 10e-4);
%! assert(pl,1/2,1e-8)

% dve fiksni tocki, ena kroznica, drug radij ter pozicija
%!test
%! K = [0, 0, 2; 2, 0, 0; 0, 0, 1.5];
%! [T, pl] = trikotnik(K, 10e-4);
%! assert(pl,1/2,1e-8)

% dve fiksni tocki, ena kroznica, ki ima sredisce na razpoloviscu, ki ga
% tvorita fiksni tocki
%!test
%! K = [-1, 0, -0.5; 1, 0, 0.5; 0, 0, sqrt(2)/2];
%! [T, pl] = trikotnik(K, 10e-2);
%! assert(pl,1/2,1e-8)

function trikotnik_izris(K, xmin, xint)
% prikazemo potek iskanja vrednosti
  figure(1);
  hold on
  plot3(xint(1,1),xint(2,1),xint(3,1),'ro');
  plot3(xint(1,end),xint(2,end),xint(3,end),'go');
  legend('zacetek','konec');
  
  plot3(xint(1,:),xint(2,:),xint(3,:),'-');
  view(-45,25);
  
  title("Razvoj vrednosti gradienta pri metodi najhitrejsega spusta");
  hold off
  axis on
  grid on
  
  axis equal
  
  % izrisemo kroznice
  figure(2);
  hold on
  [Ax, Ay] = kroznica(K(:,1), xmin(1),'t');
  [Bx, By] = kroznica(K(:,2), xmin(2),'u');
  [Cx, Cy] = kroznica(K(:,3), xmin(3),'v');

  % izrisemo trikotnik
  fill([Ax, Bx, Cx, Ax],[Ay, By, Cy, Ay],[187/255, 1, 185/255]');
  
  hold off
  
  axis on
  grid on
  
  axis equal
endfunction

% parametrizacija tocke
function pp = p(K, t)
  pp = [K(1)+K(3)*cos(t); K(2)+K(3)*sin(t)];
endfunction

% odvod parametrizacije tocke
function dtp = pdot(K, t)
  dtp = [-K(3)*sin(t); K(3)*cos(t)];
endfunction

% funkcija za izracun posamezne stranice
function aval = a(K1, K2, tuv)
  aval = p(K2,tuv(2))-p(K1,tuv(1));
endfunction

function bval = b(K1, K3, tuv)
  bval = p(K3,tuv(3))-p(K1,tuv(1));
endfunction

% ploscina trikotnika
function P = trik_ploscina(K1, K2, K3, tuv)
  % vzamemo absolutno vrednost, ker je lahko zaradi negativne orientacije
  % rezultat negativen
  P = abs(det([a(K1, K2, tuv), b(K1, K3, tuv)])/2);
endfunction

function grad = trik_ploscina_grad(K1, K2, K3, tuv)
  detab = det([a(K1, K2, tuv), b(K1, K3, tuv)]);
  grad = -[ % odvod po t
            detab*(det([-pdot(K1,tuv(1)), b(K1, K3, tuv)]) + det([a(K1, K2, tuv), -pdot(K1,tuv(1))])),
            % odvod po u
            detab*(det([pdot(K2,tuv(2)),  b(K1, K3, tuv)])),
            % odvod po v
            detab*(det([a(K1, K2, tuv), pdot(K3,tuv(3))]))
          ];
endfunction

function [x, k, xint] = gradmet(gradf, h, x0, tol, maxit)
  %[x, k] = gradmet(gradf, h, x0, tol, maxit) vrne lokalni minimum x
  %funkcije f z gradientom gradf, ki ga poisce z metodo najhitrejsega 
  %spusta. 
  %gradf ... gradient funkcije f
  %h ... korak
  %x0 ... zacetni priblizek
  %tol ... zahtevana natancnost
  %maxit ... najvecje stevilo iteracij

  % pnrom - norma prejsnjega gradienta, katerega razliko s trenutnim
  % uporabimo za toleranco
  pnorm=0;
  
  xint=[];
  %glavna zanka metode najhitrejsega spusta
  for k = 1:maxit
    grad0 = feval(gradf, x0);
    
    tnorm = norm(grad0);
    % dolocimo premik - pri tem ne premikamo, ce je trenutna norma 0
    % (da se izognemo -Inf pri logaritmu)
    % pri tem zlorabljam kratkosticno evalvacijo izrazov pri pogojih ter
    % dejstvo, da lahko Octave prireja vrednost v spremenljivko znotraj
    % izrazov
    tnorm!=0 && (mul = 10^(ceil(-log10(tnorm)))) || (mul = 0);
    
    % vrednost obravnavamo za konvergirano sele takrat, ko je razlika norm
    % gradientov prejsnjega ter trenutnega cikla manjsa od tolerance
    % (tj. ce je novi premik manjsi od se sprejemljive napake, kar pomeni, da
    % smo dovolj blizu prave resitve)
    if(k>1 && (abs(tnorm*mul-pnorm*mul) < tol))
      break;
    else
      % bodisi se nismo prisli do konca ali pa smo prvic
      pnorm = tnorm;
    end
    x = x0 - h*grad0;
    xint = [xint, x];
    x0 = x;
  end
  
  if(k==maxit)
    printf("Pozor: metoda ni konvergirala!\n");
  end
endfunction

function [x,y] = kroznica(K, pt, znak)
  p = K(1);
  q = K(2);
  r = K(3);
  % narisemo kroznico ter tocko ter vrnemo koordinate tock nazaj
  t = linspace(0, 2*pi, 200);
  plot(p+r*cos(t), q+r*sin(t), 'b-');
  x = p+r*cos(pt);
  y = q+r*sin(pt);
  plot(x,y,'ro');
  % na zunanjem radiju izpisemo se parameter
  xod=r/15;
  yod=r/15;
  align='left';
  if(x<p)
    align='right';
  end
  text(p+(xod+r)*cos(pt), q+(yod+r)*sin(pt), sprintf("%s=%d",znak,pt),'horizontalalignment',align);
endfunction